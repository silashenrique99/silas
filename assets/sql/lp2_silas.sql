-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 21-Nov-2019 às 21:56
-- Versão do servidor: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lp2_silas`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `componente_item`
--

CREATE TABLE `componente_item` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `conteudo` text,
  `componente_list_id` mediumint(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `componente_item`
--

INSERT INTO `componente_item` (`id`, `conteudo`, `componente_list_id`) VALUES
(1, '<div class="container mt-5">\r\n    <div class="row">\r\n        <div class="col-md">\r\n\r\n<h1>Fixed Menu</h1>\r\n<h2>Aprenda a criar um menu "fixo" com CSS.</h2>\r\n\r\n\r\n<h3>Como criar um menu superior fixo</h3>\r\n<h4>Etapa 1) Adicione HTML:</h4>\r\n\r\n\r\n<form>\r\n<textarea cols="40" rows="10">\r\n<div class="navbar">\r\n <a href="#home">Home</a>\r\n <a href="#news">Noticias</a>\r\n <a href="#contact">Contato</a>\r\n </div>\r\n\r\n <div class="main">\r\n <p>Algum texto algum texto algum texto algum texto...</p>\r\n</div>\r\n</textarea>\r\n</form>\r\n\r\n<hr></hr>\r\n\r\n\r\n<h4>Etapa 2) Adicione CSS:</h4>\r\n<p>Para criar um menu superior fixo, use position:fixede top:0. \r\nObserve que o menu fixo irá sobrepor seu outro conteúdo.\r\nPara corrigir isso, adicione um margin-top(ao conteúdo) \r\nigual ou maior que a altura do seu menu.</p>\r\n\r\n\r\n<form>\r\n<textarea cols="60" rows="30">\r\n/* The navigation bar */\r\n.navbar {\r\n  overflow: hidden;\r\n  background-color: #333;\r\n  position: fixed; /* Set the navbar to fixed position */\r\n  top: 0; /* Position the navbar at the top of the page */\r\n  width: 100%; /* Full width */\r\n}\r\n\r\n/* Links inside the navbar */\r\n.navbar a {\r\n  float: left;\r\n  display: block;\r\n  color: #f2f2f2;\r\n  text-align: center;\r\n  padding: 14px 16px;\r\n  text-decoration: none;\r\n}\r\n\r\n/* Change background on mouse-over */\r\n.navbar a:hover {\r\n  background: #ddd;\r\n  color: black;\r\n}\r\n\r\n/* Main content */\r\n.main {\r\n  margin-top: 30px; /* Add a top margin to avoid content overlay */\r\n}\r\n</textarea>\r\n</form>\r\n\r\n</div>\r\n</div>\r\n</div>\r\n', NULL),
(2, '<div class="container mt-5">\r\n    <div class="row">\r\n        <div class="col-md">\r\n\r\n<h1>Carrosel</h1>\r\n<h2>Aprenda a criar um menu "fixo" com CSS.</h2>\r\n\r\n\r\n<h3>Como criar um menu superior fixo</h3>\r\n<h4>Etapa 1) Adicione HTML:</h4>\r\n\r\n\r\n<form>\r\n<textarea cols="40" rows="10">\r\n<div class="navbar">\r\n <a href="#home">Home</a>\r\n <a href="#news">Noticias</a>\r\n <a href="#contact">Contato</a>\r\n </div>\r\n\r\n <div class="main">\r\n <p>Algum texto algum texto algum texto algum texto...</p>\r\n</div>\r\n</textarea>\r\n</form>\r\n\r\n<hr></hr>\r\n\r\n\r\n<h4>Etapa 2) Adicione CSS:</h4>\r\n<p>Para criar um menu superior fixo, use position:fixede top:0. \r\nObserve que o menu fixo irá sobrepor seu outro conteúdo.\r\nPara corrigir isso, adicione um margin-top(ao conteúdo) \r\nigual ou maior que a altura do seu menu.</p>\r\n\r\n\r\n<form>\r\n<textarea cols="60" rows="30">\r\n/* The navigation bar */\r\n.navbar {\r\n  overflow: hidden;\r\n  background-color: #333;\r\n  position: fixed; /* Set the navbar to fixed position */\r\n  top: 0; /* Position the navbar at the top of the page */\r\n  width: 100%; /* Full width */\r\n}\r\n\r\n/* Links inside the navbar */\r\n.navbar a {\r\n  float: left;\r\n  display: block;\r\n  color: #f2f2f2;\r\n  text-align: center;\r\n  padding: 14px 16px;\r\n  text-decoration: none;\r\n}\r\n\r\n/* Change background on mouse-over */\r\n.navbar a:hover {\r\n  background: #ddd;\r\n  color: black;\r\n}\r\n\r\n/* Main content */\r\n.main {\r\n  margin-top: 30px; /* Add a top margin to avoid content overlay */\r\n}\r\n</textarea>\r\n</form>\r\n\r\n</div>\r\n</div>\r\n</div>\r\n', NULL),
(3, '<div class="container mt-5">\r\n    <div class="row">\r\n        <div class="col-md">\r\n\r\n<h1>Fixed Menu</h1>\r\n<h2>Aprenda a criar um menu "fixo" com CSS.</h2>\r\n\r\n\r\n<h3>Como criar um menu superior fixo</h3>\r\n<h4>Etapa 1) Adicione HTML:</h4>\r\n\r\n\r\n<form>\r\n<textarea cols="40" rows="10">\r\n<div class="navbar">\r\n <a href="#home">Home</a>\r\n <a href="#news">Noticias</a>\r\n <a href="#contact">Contato</a>\r\n </div>\r\n\r\n <div class="main">\r\n <p>Algum texto algum texto algum texto algum texto...</p>\r\n</div>\r\n</textarea>\r\n</form>\r\n\r\n<hr></hr>\r\n\r\n\r\n<h4>Etapa 2) Adicione CSS:</h4>\r\n<p>Para criar um menu superior fixo, use position:fixede top:0. \r\nObserve que o menu fixo irá sobrepor seu outro conteúdo.\r\nPara corrigir isso, adicione um margin-top(ao conteúdo) \r\nigual ou maior que a altura do seu menu.</p>\r\n\r\n\r\n<form>\r\n<textarea cols="60" rows="30">\r\n/* The navigation bar */\r\n.navbar {\r\n  overflow: hidden;\r\n  background-color: #333;\r\n  position: fixed; /* Set the navbar to fixed position */\r\n  top: 0; /* Position the navbar at the top of the page */\r\n  width: 100%; /* Full width */\r\n}\r\n\r\n/* Links inside the navbar */\r\n.navbar a {\r\n  float: left;\r\n  display: block;\r\n  color: #f2f2f2;\r\n  text-align: center;\r\n  padding: 14px 16px;\r\n  text-decoration: none;\r\n}\r\n\r\n/* Change background on mouse-over */\r\n.navbar a:hover {\r\n  background: #ddd;\r\n  color: black;\r\n}\r\n\r\n/* Main content */\r\n.main {\r\n  margin-top: 30px; /* Add a top margin to avoid content overlay */\r\n}\r\n</textarea>\r\n</form>\r\n\r\n</div>\r\n</div>\r\n</div>\r\n', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `componente_list`
--

CREATE TABLE `componente_list` (
  `id` int(11) NOT NULL,
  `componente` varchar(100) NOT NULL,
  `conteudo` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `componente_list`
--

INSERT INTO `componente_list` (`id`, `componente`, `conteudo`) VALUES
(1, 'Fixed Menu', '<div class="container mt-5">\r\n    <div class="row">\r\n        <div class="col-md">\r\n\r\n<h1>Fixed Menu</h1>\r\n<h2>Aprenda a criar um menu "fixo" com CSS.</h2>\r\n\r\n\r\n<h3>Como criar um menu superior fixo</h3>\r\n<h4>Etapa 1) Adicione HTML:</h4>\r\n\r\n\r\n<form>\r\n<textarea cols="40" rows="10">\r\n<div class="navbar">\r\n <a href="#home">Home</a>\r\n <a href="#news">Noticias</a>\r\n <a href="#contact">Contato</a>\r\n </div>\r\n\r\n <div class="main">\r\n <p>Algum texto algum texto algum texto algum texto...</p>\r\n</div>\r\n</textarea>\r\n</form>\r\n\r\n<hr></hr>\r\n\r\n\r\n<h4>Etapa 2) Adicione CSS:</h4>\r\n<p>Para criar um menu superior fixo, use position:fixede top:0. \r\nObserve que o menu fixo irá sobrepor seu outro conteúdo.\r\nPara corrigir isso, adicione um margin-top(ao conteúdo) \r\nigual ou maior que a altura do seu menu.</p>\r\n\r\n\r\n<form>\r\n<textarea cols="60" rows="30">\r\n/* The navigation bar */\r\n.navbar {\r\n  overflow: hidden;\r\n  background-color: #333;\r\n  position: fixed; /* Set the navbar to fixed position */\r\n  top: 0; /* Position the navbar at the top of the page */\r\n  width: 100%; /* Full width */\r\n}\r\n\r\n/* Links inside the navbar */\r\n.navbar a {\r\n  float: left;\r\n  display: block;\r\n  color: #f2f2f2;\r\n  text-align: center;\r\n  padding: 14px 16px;\r\n  text-decoration: none;\r\n}\r\n\r\n/* Change background on mouse-over */\r\n.navbar a:hover {\r\n  background: #ddd;\r\n  color: black;\r\n}\r\n\r\n/* Main content */\r\n.main {\r\n  margin-top: 30px; /* Add a top margin to avoid content overlay */\r\n}\r\n</textarea>\r\n</form>\r\n\r\n</div>\r\n</div>\r\n</div>\r\n'),
(2, 'Carrosel', '<div class="container mt-5">\r\n    <div class="row">\r\n        <div class="col-md">\r\n\r\n<h1>Carrosel</h1>\r\n<h2>Aprenda a criar um menu "fixo" com CSS.</h2>\r\n\r\n\r\n<h3>Como criar um menu superior fixo</h3>\r\n<h4>Etapa 1) Adicione HTML:</h4>\r\n\r\n\r\n<form>\r\n<textarea cols="40" rows="10">\r\n<div class="navbar">\r\n <a href="#home">Home</a>\r\n <a href="#news">Noticias</a>\r\n <a href="#contact">Contato</a>\r\n </div>\r\n\r\n <div class="main">\r\n <p>Algum texto algum texto algum texto algum texto...</p>\r\n</div>\r\n</textarea>\r\n</form>\r\n\r\n<hr></hr>\r\n\r\n\r\n<h4>Etapa 2) Adicione CSS:</h4>\r\n<p>Para criar um menu superior fixo, use position:fixede top:0. \r\nObserve que o menu fixo irá sobrepor seu outro conteúdo.\r\nPara corrigir isso, adicione um margin-top(ao conteúdo) \r\nigual ou maior que a altura do seu menu.</p>\r\n\r\n\r\n<form>\r\n<textarea cols="60" rows="30">\r\n/* The navigation bar */\r\n.navbar {\r\n  overflow: hidden;\r\n  background-color: #333;\r\n  position: fixed; /* Set the navbar to fixed position */\r\n  top: 0; /* Position the navbar at the top of the page */\r\n  width: 100%; /* Full width */\r\n}\r\n\r\n/* Links inside the navbar */\r\n.navbar a {\r\n  float: left;\r\n  display: block;\r\n  color: #f2f2f2;\r\n  text-align: center;\r\n  padding: 14px 16px;\r\n  text-decoration: none;\r\n}\r\n\r\n/* Change background on mouse-over */\r\n.navbar a:hover {\r\n  background: #ddd;\r\n  color: black;\r\n}\r\n\r\n/* Main content */\r\n.main {\r\n  margin-top: 30px; /* Add a top margin to avoid content overlay */\r\n}\r\n</textarea>\r\n</form>\r\n\r\n</div>\r\n</div>\r\n</div>\r\n'),
(3, 'Form->input fields', '<div class="container mt-5">\r\n    <div class="row">\r\n        <div class="col-md">\r\n\r\n<h1>input</h1>\r\n<h2>Aprenda a criar um menu "fixo" com CSS.</h2>\r\n\r\n\r\n<h3>Como criar um menu superior fixo</h3>\r\n<h4>Etapa 1) Adicione HTML:</h4>\r\n\r\n\r\n<form>\r\n<textarea cols="40" rows="10">\r\n<div class="navbar">\r\n <a href="#home">Home</a>\r\n <a href="#news">Noticias</a>\r\n <a href="#contact">Contato</a>\r\n </div>\r\n\r\n <div class="main">\r\n <p>Algum texto algum texto algum texto algum texto...</p>\r\n</div>\r\n</textarea>\r\n</form>\r\n\r\n<hr></hr>\r\n\r\n\r\n<h4>Etapa 2) Adicione CSS:</h4>\r\n<p>Para criar um menu superior fixo, use position:fixede top:0. \r\nObserve que o menu fixo irá sobrepor seu outro conteúdo.\r\nPara corrigir isso, adicione um margin-top(ao conteúdo) \r\nigual ou maior que a altura do seu menu.</p>\r\n\r\n\r\n<form>\r\n<textarea cols="60" rows="30">\r\n/* The navigation bar */\r\n.navbar {\r\n  overflow: hidden;\r\n  background-color: #333;\r\n  position: fixed; /* Set the navbar to fixed position */\r\n  top: 0; /* Position the navbar at the top of the page */\r\n  width: 100%; /* Full width */\r\n}\r\n\r\n/* Links inside the navbar */\r\n.navbar a {\r\n  float: left;\r\n  display: block;\r\n  color: #f2f2f2;\r\n  text-align: center;\r\n  padding: 14px 16px;\r\n  text-decoration: none;\r\n}\r\n\r\n/* Change background on mouse-over */\r\n.navbar a:hover {\r\n  background: #ddd;\r\n  color: black;\r\n}\r\n\r\n/* Main content */\r\n.main {\r\n  margin-top: 30px; /* Add a top margin to avoid content overlay */\r\n}\r\n</textarea>\r\n</form>\r\n\r\n</div>\r\n</div>\r\n</div>\r\n');

-- --------------------------------------------------------

--
-- Estrutura da tabela `dados_pessoais`
--

CREATE TABLE `dados_pessoais` (
  `id` int(11) NOT NULL,
  `nome` varchar(20) NOT NULL,
  `sobrenome` varchar(100) NOT NULL,
  `sexo` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `dados_pessoais`
--

INSERT INTO `dados_pessoais` (`id`, `nome`, `sobrenome`, `sexo`) VALUES
(1, 'Zero', 'Shirotsuki', 2),
(2, 'Shin', 'Sakamitsu', 2),
(3, 'Lúpus', 'Remmiki', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `endereco`
--

CREATE TABLE `endereco` (
  `id` int(11) NOT NULL,
  `id_funcionario` int(11) NOT NULL,
  `rua` varchar(100) NOT NULL,
  `bairro` varchar(100) NOT NULL,
  `cidade` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `endereco`
--

INSERT INTO `endereco` (`id`, `id_funcionario`, `rua`, `bairro`, `cidade`, `estado`) VALUES
(1, 1, 'Av. Salgado Filho, 2000', 'Vila Rio de Janeiro', 'Guarulhos', 'São Paulo'),
(2, 2, 'Av. Azedo Filho', 'Vila Rio de Janeiro', 'Guarulhos', 'São Paulo'),
(3, 3, 'Av. Tiradentes, 1000', 'Centro', 'São Paulo', 'São Paulo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `id` int(11) NOT NULL,
  `nome` varchar(20) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `preco` decimal(8,2) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`id`, `nome`, `descricao`, `preco`, `last_modified`) VALUES
(1, 'Teste', 'Um teste qualquer feito para ver se está tudo funcionando perfeitamente', '18.90', '2019-09-23 14:32:03'),
(4, 'Sabão', 'Passar no corpo', '6.77', '2019-09-10 00:40:41'),
(8, 'Sapatos', 'Para proteger os pés', '5.86', '2019-09-10 01:15:06'),
(9, 'Ut Nec Incorporated', 'ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede et risus. Quisque libero lacus, varius', '8.26', '2019-09-02 23:39:40'),
(10, 'Enim Corp.', 'sapien molestie orci tincidunt adipiscing. Mauris molestie pharetra nibh. Aliquam ornare, libero at auctor ullamcorper, nisl', '8.67', '2019-09-02 23:39:40'),
(11, 'Quisque Varius Assoc', 'sollicitudin adipiscing ligula. Aenean gravida nunc sed pede. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur', '7.78', '2019-09-02 23:39:40'),
(12, 'Egestas Aliquam PC', 'eu eros. Nam consequat dolor vitae dolor. Donec fringilla. Donec', '4.05', '2019-09-02 23:39:40'),
(13, 'Magnis Company', 'nibh lacinia orci, consectetuer euismod est arcu ac orci. Ut semper pretium neque. Morbi quis urna. Nunc quis', '3.15', '2019-09-02 23:39:40'),
(14, 'Et Company', 'libero. Donec consectetuer mauris id sapien. Cras dolor dolor, tempus non, lacinia at, iaculis quis,', '2.29', '2019-09-02 23:39:40'),
(15, 'Donec At Corp.', 'ultricies adipiscing, enim mi tempor lorem, eget mollis lectus pede et risus. Quisque libero lacus, varius et,', '6.23', '2019-09-02 23:39:40'),
(16, 'Purus Nullam Limited', 'neque pellentesque massa lobortis ultrices. Vivamus rhoncus. Donec est. Nunc ullamcorper, velit in aliquet lobortis, nisi', '9.65', '2019-09-02 23:39:40'),
(17, 'Rhoncus Proin Consul', 'sem. Nulla interdum. Curabitur dictum. Phasellus in felis. Nulla tempor augue ac ipsum. Phasellus', '1.27', '2019-09-02 23:39:40'),
(18, 'Suspendisse Ltd', 'egestas. Fusce aliquet magna a neque. Nullam ut nisi a odio semper cursus. Integer mollis. Integer tincidunt aliquam arcu. Aliquam', '6.68', '2019-09-02 23:39:40'),
(19, 'Sed LLC', 'lacinia orci, consectetuer euismod est arcu ac orci. Ut semper pretium neque. Morbi quis urna. Nunc', '1.29', '2019-09-02 23:39:40'),
(20, 'Vestibulum Accumsan ', 'vitae velit egestas lacinia. Sed congue, elit sed consequat auctor, nunc', '1.38', '2019-09-02 23:39:40'),
(21, 'Salame', 'Carne processada', '20.00', '2019-09-09 23:26:07'),
(22, 'Abacaxi', 'Uma fruta amarela extremamente deliciosa', '18.00', '2019-10-03 04:51:34');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `componente_item`
--
ALTER TABLE `componente_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `componente_list`
--
ALTER TABLE `componente_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dados_pessoais`
--
ALTER TABLE `dados_pessoais`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `endereco`
--
ALTER TABLE `endereco`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `componente_item`
--
ALTER TABLE `componente_item`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `componente_list`
--
ALTER TABLE `componente_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `dados_pessoais`
--
ALTER TABLE `dados_pessoais`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `endereco`
--
ALTER TABLE `endereco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
