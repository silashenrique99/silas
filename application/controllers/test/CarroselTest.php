<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');
include_once APPPATH. 'libraries/CarroselData.php';


class CarroselTest extends MyToast {

    function __construct() {
        parent::__construct('CarroselTest');
    }

    //Caso de teste 1 não vazia
    public function test_slide1(){
		$carrosel = New CarroselData('Carrosel');
        $data = $carrosel->getSlide1();
        $dados = $data;
        $this->_assert_not_empty($dados, 'Erro variavel vazia!');

    }

    //Caso de teste 2 não vazia
    public function test_slide2(){
		$carrosel = New CarroselData('Carrosel');
        $data = $carrosel->getSlide2();
        $dados = $data;
        $this->_assert_not_empty($dados, 'Erro variavel vazia!');

    }

    //Caso de teste 3 não vazia
    public function test_slide3(){
		$carrosel = New CarroselData('Carrosel');
        $data = $carrosel->getSlide3();
        $dados = $data;
        $this->_assert_not_empty($dados, 'Erro variavel vazia!');

    }
    
} 