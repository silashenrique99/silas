<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');
include_once APPPATH. 'libraries/FixedMenuData.php';

class FixedMenuTest extends MyToast {

    function __construct() {
        parent::__construct('FixedMenuTest');
    }

    //Caso de teste 4 não vazia
    public function test_fixedmenu(){
		$fixedmenu = New FixedMenuData('fixedmenu');
        $data = $fixedmenu->getFixedMenu();
        $dados = $data;
        $this->_assert_not_empty($dados, 'Erro variavel vazia!');

    }
    
} 