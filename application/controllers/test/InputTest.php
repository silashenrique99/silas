<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH . '/controllers/test/MyToast.php');
include_once APPPATH. 'libraries/InputData.php';

class InputTest extends MyToast {

    function __construct() {
        parent::__construct('InputTest');
    }

    //Caso de teste 5 não vazia
    public function test_input(){
		$input = New InputData('input');
        $data = $input->getMaterial();
        $dados = $data;
        $this->_assert_not_empty($dados, 'Erro variavel vazia!');

    }
    
} 