<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Componentes extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('ComponenteModel', 'componente');
        $this->load->model('CadastroModel', 'cadastro');
        
    }

    public function index(){
        $html = $this->load->view('componente/visualiza',null, true);
        $this->show($html);
    }

    public function FixedMenu(){
        $html = $this->load->view('componente/fixedmenu', null, true);
        $this->show($html);
    }

    public function FixedMenuExemplo(){

        $data['fixedmenu'] = $this->componente->getFixedMenu();
        $html = $this->load->view('componente/exemplofixedmenu', $data, true);
        $this->exemplo($html);
    }

    public function Carrosel(){
        $data['slide1'] = $this->componente->getSlide1();
        $data['slide2'] = $this->componente->getSlide2();
        $data['slide3'] = $this->componente->getSlide3();
        $html = $this->load->view('componente/carrosel', $data, true);
        $this->show($html);
    }

    public function CarroselExemplo(){
        $html = $this->load->view('componente/carroselexemplo', null, true);
        $this->exemplo($html);
    }
    
    
    public function Input(){
        $data['material'] = $this->componente->getInputMaterial();
        $data['padrao'] = $this->componente->getInputPadrao();

        $html = $this->load->view('componente/input', $data, true);
        $this->show($html);
    }

    public function InputExemplo(){
        $html = $this->load->view('componente/inputexemplo', null, true);
        $this->exemplo($html);
    }

    public function Contato(){
        $this->cadastro->salva();
        $html = $this->load->view('componente/contato', null, true);
        $this->show($html);
        
    }

    public function Login(){
        
        $html = $this->load->view('componente/login', null, true);
        $this->show($html);
    }



}