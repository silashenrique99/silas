<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Loja extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('CadastroModel', 'cadastro');

    }
 

    public function Contato(){
        $this->cadastro->salva();
        $html = $this->load->view('componente/contato', null, true);
        $this->show($html);
        
    }

    public function Login(){
        
        $html = $this->load->view('componente/login', null, true);
        $this->show($html);
    }
}
