<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller{

    public function show($conteudo){
        //cabeçalho
        $html = $this->load->view('common/header', null, true);
        $html .= $this->load->view('common/navbar', null, true);
        
        
        //conteudo
        $html .= $conteudo;


        //rodape
        
        $html .= $this->load->view('componente/rodape', null, true);
        $html .= $this->load->view('common/footer', null, true);

        echo $html;
    }

    public function exemplo($conteudo){
        $html = $this->load->view('componente/header', null, true);
        $html .= $conteudo;

        echo $html;
    }

}