<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once 'ComponenteItem.php';
include_once 'ComponenteData.php';

class ComponenteLoader extends CI_Object{
    private $componente_item_list;
    
    function __construct($index = 0){
        if($index) $this->getComponenteData($index);
    }

    private function getComponenteData($index){
        $sql = "SELECT CONCAT(nome, ' ', sobrenome) AS titulo, 
        imagem, conteudo FROM componente_item WHERE 
        componente_list_id = $index";
        $rs = $this->db->query($sql);
        $this->componente_item_list = $rs->result();
    }

    public function getHTML(){
        $html = ''; $i = 0;
        foreach ($this->componente_item_list AS $item) {
            $data = new ComponenteData($item);
            $item = new ComponenteItem($data);
            $html .= $item->getHTML();
        }
        return $html;
    }

    public function getSubjectList(){
        $rs = $this->db->get('componente_list');
        return $rs->result();
    }

}