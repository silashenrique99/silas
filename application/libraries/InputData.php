<?php 
/**
* classe que representa as variaveis manipuladas para construir o Inputs Fields.
*/
class InputData {
    // atributos
    private $material;
    private $padrao;


    // construtor
    function __construct(){
        $this->material = '
        <!-- Large input -->
        <div class="md-form form-lg">
          <input type="text" id="inputLGEx" class="form-control form-control-lg">
          <label for="inputLGEx">Large input</label>
        </div>
        
        <!-- Medium input -->
        <div class="md-form">
          <input type="text" id="inputMDEx" class="form-control">
          <label for="inputMDEx">Medium input</label>
        </div>
        
        <!-- Small input -->
        <div class="md-form form-sm">
          <input type="text" id="inputSMEx" class="form-control form-control-sm">
          <label for="inputSMEx">Small input</label>
        </div>
        ';
        
        $this->padrao = '<!-- Large input -->
        <input class="form-control form-control-lg" type="text" placeholder="Large input">
        
        <!-- Small input -->
        <input class="form-control" type="text" placeholder="Medium input">
        
        <!-- Small input -->
        <input class="form-control form-control-sm" type="text" placeholder="Small input">';

        
    }

    
    /**
     * Constroi a entrada material.
     * @return material: string | -1 em caso de erro;
     */
    public function getMaterial(){
        return $this->material;
    }

    /**
     * Constroi a entrada padrão.
     * @return padrao: string | -1 em caso de erro;
     */
    public function getPadrao(){
        return $this->padrao;
    }
    
}