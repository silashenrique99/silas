<?php 

/**
 * classe que representa as variaveis manipuladas para construir o FixedMenu.
 */

class FixedMenuData {
    // atributos
    private $fixedmenu;


    // construtor
    function __construct(){
        $this->fixedmenu = '<div class="container">
        <div class="row">
                <div class="col-md">
        <div class="navbar">
          <a href="#home">Home</a>
          <a href="#news">News</a>
          <a href="#contact">Contact</a>
        </div>
        
        <div class="main">
          <p>Some text some text some text some text..</p>
        </div>
        
        </div>
        </div>
        </div>
        
        </body>
        </head>
        </html>';
        
    }

     /**
     * Constroi o fixed menu.
     * @return fixedmenu: string | -1 em caso de erro;
     */

    public function getFixedMenu(){
        return $this->fixedmenu;
    }
    
}