<?php 


/**
 * classe representa as variaveis manipuladas em cada carrosel.
 */

class CarroselData {
    // atributos
    private $slide1;
    private $slide2;
    private $slide3;


    // construtor
    function __construct(){
        
        $this->slide1 = '<div class="container">
        <div class="row">
        <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(35).jpg">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(33).jpg">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(31).jpg">
            </div>
          </div>
        </div>
        </div>
        </div>  
        ';

        $this->slide2 = '<div class="container">
        <div class="row">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(45).jpg"
                alt="First slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(46).jpg"
                alt="Second slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(47).jpg"
                alt="Third slide">
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        </div>
        </div>';

        $this->slide3 = '<div class="container">
        <div class="row">
        <!--Carousel Wrapper-->
        <div id="video-carousel-example" class="carousel slide carousel-fade" data-ride="carousel">
          <!--Indicators-->
          <ol class="carousel-indicators">
            <li data-target="#video-carousel-example" data-slide-to="0" class="active"></li>
            <li data-target="#video-carousel-example" data-slide-to="1"></li>
            <li data-target="#video-carousel-example" data-slide-to="2"></li>
          </ol>
          <!--/.Indicators-->
          <!--Slides-->
          <div class="carousel-inner" role="listbox">
            <div class="carousel-item active">
              <video class="video-fluid" autoplay loop muted>
                <source src="https://mdbootstrap.com/img/video/Tropical.mp4" type="video/mp4" />
              </video>
            </div>
            <div class="carousel-item">
              <video class="video-fluid" autoplay loop muted>
                <source src="https://mdbootstrap.com/img/video/forest.mp4" type="video/mp4" />
              </video>
            </div>
            <div class="carousel-item">
              <video class="video-fluid" autoplay loop muted>
                <source src="https://mdbootstrap.com/img/video/Agua-natural.mp4" type="video/mp4" />
              </video>
            </div>
          </div>
          <!--/.Slides-->
          <!--Controls-->
          <a class="carousel-control-prev" href="#video-carousel-example" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#video-carousel-example" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
          <!--/.Controls-->
        </div>
        <!--Carousel Wrapper-->
        </div>
        </div>';
       
    }

    /**
     * Constroi o primeiro carrosel.
     * @return slide1: string | -1 em caso de erro;
     */

    public function getSlide1(){
        return $this->slide1;
    }

     /**
     * Constroi o segundo carrosel.
     * @return slide2: string | -1 em caso de erro;
     */    
    public function getSlide2(){
        return $this->slide2;
    }

     /**
     * Constroi o terceiro carrosel.
     * @return slide3: string | -1 em caso de erro;
     */
    public function getSlide3(){
        return $this->slide3;
    }
    
}