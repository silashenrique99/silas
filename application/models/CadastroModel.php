<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH.'libraries/component/Table.php';

class CadastroModel extends CI_Model{

    function __construct(){
        parent::__construct();
     
        $this->load->model('CadastroModel', 'cadastro');
    }

    public function salva(){

        
        if($this->validate()){
            $this->db->trans_start();

            
            $dados = $this->input->post('dados');
            $this->load->library('DadosPessoais', null, 'dados');
            $id = $this->dados->insere($dados);

        
            $end = $this->input->post('endereco');
            $this->load->library('Endereco');
            $this->endereco->insere($end, $id);
            $this->salva_curriculo($id);
        

            $this->db->trans_complete();
            if($this->db->trans_status()){
                

            }else{


            }



        }
        
    }

    private function validate(){
        $this->load->library('util/Validator');

        $this->validator->valida_dados_pessoais();
        $this->validator->valida_endereco();
        return $this->form_validation->run();

    }

    public function tabela(){
        $this->load->library('DadosPessoais', null, 'dados');
        $labels = array('Nome', 'Sobrenome', 'Sexo', 'email');
        $data = $this->dados->getAll();
        foreach ($data as $key => $val){
            $data[$key]['botoes'] = $this->action_buttons($val);
            //unset($data[$key]['id']);

        }


        $table = new Table($data, $labels);
        return $table->getHTML();

    }

    private function action_buttons($row){
        $html = '<a href="'.base_url('index.php/cadastro/edita/'.$row['id']).'"><i class="fas fa-edit mr-3 blue-text" title="Editar" ></i></a>';
        $html .= '<a href="'.base_url('index.php/cadastro/delete/'.$row['id']).'"><i class="fas fa-trash mr-3" red-text title="Deletar" ></i></a>';
        $html .= '<a href="'.base_url('index.php/curriculo/curriculo_'.$row['id']).'.pdf" target="_blank"><i class="far fa-file-pdf gray-text" title="Visualizar Curriculo"></i></a>';
        return $html;

    }


}