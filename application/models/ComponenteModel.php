<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/CarroselData.php';
include APPPATH.'libraries/FixedMenuData.php';
include APPPATH.'libraries/InputData.php';

class ComponenteModel extends CI_Model{

    public function getSlide1(){
        $carrosel = new CarroselData();
        $slide1 = $carrosel->getSlide1();
        return $slide1;
    }

    public function getSlide2(){
        $carrosel = new CarroselData();
        $slide2 = $carrosel->getSlide2();
        return $slide2;

    }

    public function getSlide3(){
        $carrosel = new CarroselData();
        $slide3 = $carrosel->getSlide3();

        return $slide3;
    }

    public function getFixedMenu(){
        $fixedmenu = new FixedMenuData();
        $data = $fixedmenu->getFixedMenu();
        return $data;
    }
    
    
    
        public function getInputMaterial(){
            $input = new InputData();
            $data = $input->getMaterial();
            return $data;
        }

        
        public function getInputPadrao(){
            $input = new InputData();
            $data = $input->getPadrao();
            return $data;
        }

}