<div class="container mt-5">
    <div class="row">
        <div class="col-md">


<h1>Input Fields</h1>
<h4>Input Fields são campos em que você pode colocar seus dados e usar tamanhos diferentes de entradas.</h4>

</br>
</br>
<div class="container">
<h5>Entrada material</h5>
<?= $material?>
</div>

</br>

<form>
<div >
HTML
 </div>
 <textarea cols="20" rows="15" class="d-block w-100" style="border: 2px solid black; background-color:#eee;">
    <div class="md-form form-lg">
          <input type="text" id="inputLGEx" class="form-control form-control-lg">
          <label for="inputLGEx">Large input</label>
        </div>

        <div class="md-form">
          <input type="text" id="inputMDEx" class="form-control">
          <label for="inputMDEx">Medium input</label>
        </div>
        
        <div class="md-form form-sm">
          <input type="text" id="inputSMEx" class="form-control form-control-sm">
          <label for="inputSMEx">Small input</label>
    </div>
</textarea>
</form>


</br>
</br>
<div class="container">
<h5>Entrada de padrão</h5>
<?= $padrao?>

</div>
</br>
</br>
<form>
<div >
HTML
 </div>

 <textarea cols="20" rows="4" class="d-block w-100" style="border: 2px solid black; background-color:#eee;">
    <input class="form-control form-control-lg" type="text" placeholder="Large input">
    <input class="form-control" type="text" placeholder="Medium input">
    <input class="form-control form-control-sm" type="text" placeholder="Small input">
</textarea>
</form>


</div>
</div>
</div>