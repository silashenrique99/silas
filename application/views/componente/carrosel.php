<div class="container mt-5">
    <div class="row">
        <div class="col-md">

<h1>Carrosel</h1>
<h4>Um carrosel nada mais é do que um componente de apresentação de slides que percorre imagens ou slides de texto, como se estivesse em loop por isso o nome carrosel.</h4>

<hr></hr>

<h1>Como funciona</h1>
<p>O carrossel é uma apresentação de slides para percorrer uma série de conteúdo, construído com transformações CSS 3D e um pouco de JavaScript. Funciona com uma série de imagens, texto ou marcações personalizadas. Também inclui suporte para controles e indicadores anteriores / próximos.

Nos navegadores em que a API de visibilidade da página é suportada, o carrossel evitará deslizar quando a página da web não estiver visível para o usuário (como quando a guia do navegador estiver inativa, a janela do navegador estiver minimizada etc.).

Esteja ciente de que os carrosséis aninhados não são suportados e os carrosséis geralmente não são compatíveis com os padrões de acessibilidade.</p>

<hr></hr>

<h1>Exemplo</h1>
<p>
Carrosséis não normalizam automaticamente as dimensões do slide. Como tal, pode ser necessário usar utilitários adicionais ou estilos personalizados para dimensionar adequadamente o conteúdo. Embora os carrosséis suportem controles e indicadores anteriores / próximos, eles não são explicitamente necessários. Portanto, você pode adicioná-los e personalizá-los como achar melhor.

A <b class="b">.activeclasse</b> precisa ser adicionada a um dos slides, caso contrário <b class="b">.carousel</b> , não ficará visível. Além disso, defina um ID exclusivo no <b class="b"> .carousel</b> para controles opcionais, especialmente se você estiver usando vários carrosséis em uma única página. Os elementos de controle e indicador devem ter um <b class="b">data-target</b> atributo (ou href links) que corresponda ao ID do carousel elemento.

<h4>Apenas slides</h4>
<p>Aqui está um carrossel apenas com slides. Observe a presença das imagens <b class="b">.d-blocke .w-100</b> no carrossel para impedir o alinhamento das imagens padrão do navegador.</p>

<?= $slide1 ?>


<hr></hr>

<form>
<div >
HTML
 </div>
<textarea cols="30" rows="15" class="d-block w-100" style="border: 2px solid black; background-color:#eee;">
<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(35).jpg">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(33).jpg">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(31).jpg">
    </div>
  </div>
</div>
</textarea>
</form>

<hr></hr>


<h4>Com controles</h4>
<p>Adicionando os controles anteriores e seguintes:.</p>

<?= $slide2 ?>

<hr></hr>


<form>
<div>
HTML
 </div>
<textarea cols="60" rows="30" class="d-block w-100" style="border: 2px solid black; background-color:#eee;">

<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(45).jpg"
        alt="First slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(46).jpg"
        alt="Second slide">
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(47).jpg"
        alt="Third slide">
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
</textarea>
</form>

<hr></hr>


<h4>Carrosel com video:</h4>

<?= $slide3 ?>



<form>
<div>
HTML
 </div>
<textarea cols="100" rows="40" class="d-block w-100" style="border: 2px solid black; background-color:#eee;">

<div id="video-carousel-example" class="carousel slide carousel-fade" data-ride="carousel">
  
  <ol class="carousel-indicators">
    <li data-target="#video-carousel-example" data-slide-to="0" class="active"></li>
    <li data-target="#video-carousel-example" data-slide-to="1"></li>
    <li data-target="#video-carousel-example" data-slide-to="2"></li>
  </ol>

  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <video class="video-fluid" autoplay loop muted>
        <source src="https://mdbootstrap.com/img/video/Tropical.mp4" type="video/mp4" />
      </video>
    </div>
    <div class="carousel-item">
      <video class="video-fluid" autoplay loop muted>
        <source src="https://mdbootstrap.com/img/video/forest.mp4" type="video/mp4" />
      </video>
    </div>
    <div class="carousel-item">
      <video class="video-fluid" autoplay loop muted>
        <source src="https://mdbootstrap.com/img/video/Agua-natural.mp4" type="video/mp4" />
      </video>
    </div>
  </div>
  
  <a class="carousel-control-prev" href="#video-carousel-example" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#video-carousel-example" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  
</div>
</textarea>
</form>


</div>
</div>
</div>
