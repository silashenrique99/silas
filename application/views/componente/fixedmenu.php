<div class="container mt-5">
    <div class="row">
        <div class="col-md">

<h1>Fixed Menu</h1>
<h2>Aprenda a criar um menu "fixo" com CSS.</h2>

<hr></hr>
<h4>Etapa 1) Adicione HTML:</h4>


<form>
<div>
HTML
 </div>
<textarea cols="20" rows="10" class="d-block w-100" style="border: 2px solid black; background-color:#eee;"><div class="navbar">
 <a href="#home">Home</a>
 <a href="#news">Noticias</a>
 <a href="#contact">Contato</a>
 </div>

 <div class="main">
 <p>Algum texto algum texto algum texto algum texto...</p>
</div>
</textarea>
</form>

<hr></hr>


<h4>Etapa 2) Adicione CSS:</h4>
<p>Para criar um menu superior fixo, use position:fixede top:0. 
Observe que o menu fixo irá sobrepor seu outro conteúdo.
Para corrigir isso, adicione um margin-top(ao conteúdo) 
igual ou maior que a altura do seu menu.</p>


<form>
<div>
HTML
 </div>
<textarea cols="50" rows="30" class="d-block w-100" style="border: 2px solid black; background-color:#eee;">
.navbar {
  overflow: hidden;
  background-color: #333;
  position: fixed;
  top: 0; 
  width: 100%;/
}

.navbar a {
  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}


.navbar a:hover {
  background: #ddd;
  color: black;
}

.main {
  margin-top: 30px;
}
</textarea>
</form>

</br>

<p>
<button><a href="<?=base_url('index.php/Componentes/FixedMenuExemplo')?>">Exemplo</a></button>
</p>

</div>
</div>
</div>
