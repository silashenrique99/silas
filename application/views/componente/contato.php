
<br>
<div class="container mt-5"><br>

    <div class="row">

        <div class="col-md-6 mx-auto">
<!--Section: Contact v.2-->
<section class="mb-4">
<?= validation_errors('<div class="alert alert-danger">', '</div>'); ?>
    <!--Section heading-->
    <h2 class="h1-responsive font-weight-bold text-center my-4">Entre em Contato</h2>
    <!--Section description-->
    <p class="text-center w-responsive mx-auto mb-5">
    Você tem alguma pergunta? Por favor, não hesite em nos contatar diretamente. 
    Nossa equipe entrará em contato com você em questão de horas para ajudá-lo.</p>

    <div class="row">

        <!--Grid column-->
        <div class="col-md-9 mb-md-0 mb-5">
            <form id="contact-form" name="contact-form" action="mail.php" method="POST">

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="name" name="nome" class="form-control" placeholder="Nome">
                            
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" id="sobrenome" name="sobrenome" class="form-control" placeholder="Sobrenome">
                            
                        </div>
                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row">
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                            <input type="text" id="email" name="email" class="form-control" placeholder="E-mail">
                            
                        </div>
                    </div>
                </div>
                <!--Grid row-->

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-12">

                        <div class="md-form">
                        
                            <textarea type="text" id="mensagem" name="mensagem" rows="2" class="form-control md-textarea">Mensagem</textarea>
                            
                        </div>

                    </div>
                </div>
                <!--Grid row-->

                <button class=" btn-dark my-4 col-md-6 mx-auto text-center" type="submit" >Salvar</button>

            </form>

         
            <div class="status"></div>
        </div>
        <!--Grid column-->




        <!--Grid column-->
    
        <div class="col-md-3 text-center">
            <ul class="list-unstyled mb-0">
                <li><i class="fas fa-map-marker-alt fa-2x"></i>
                    <p>Rua Monsenhor Jerônimo Rodrigues, São Paulo 226, Brasil</p>
                </li>

                <li><i class="fas fa-phone mt-4 fa-2x"></i>
                    <p>11 2334-3767</p>
                </li>

                <li><i class="fas fa-envelope mt-4 fa-2x"></i>
                    <p>silas.henrique99@outlook.com</p>
                </li>
            </ul>
        </div>
        <!--Grid column-->

    </div>

</section>
<!--Section: Contact v.2-->
</div>
</div>
</div>