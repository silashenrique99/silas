<div class="container">
    <div class="row">
        <div class="col-md-6 mx-auto">
            <p class="h4 mb-4">Endereço</p>
            <input value="<?= set_value('endereco[rua]') ?>" name="endereco[rua]" type="text" class="form-control mb-4" placeholder="Rua">
            <input value="<?= set_value('endereco[bairro]') ?>" name="endereco[bairro]" type="text" class="form-control mb-4" placeholder="Bairro">
            <input value="<?= set_value('endereco[cidade]') ?>" name="endereco[cidade]" type="text" class="form-control mb-4" placeholder="Cidade">
            <input value="<?= set_value('endereco[estado]') ?>" name="endereco[estado]" type="text" class="form-control mb-4" placeholder="Estado">
        </div>
    </div>
</div>